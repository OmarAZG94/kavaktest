package com.mtg.gnoming.net

import com.google.gson.annotations.SerializedName
import com.mtg.gnoming.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.util.concurrent.TimeUnit

open class NetworkApi {

    companion object {
        private const val SERVICE_URL = "https://raw.githubusercontent.com/rrafols/mobile_test/master/"
    }

    fun getNetworkService(): ApiService {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val customClient = OkHttpClient.Builder().readTimeout(10, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
        /* Enable interceptor only in debug mode */
        if (BuildConfig.DEBUG)
            customClient.addInterceptor(interceptor)
        val clientBuilder = customClient.build()
        val builder = Retrofit.Builder().baseUrl(SERVICE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(clientBuilder).build()
        return builder.create(ApiService::class.java)
    }

    interface ApiService {
        @GET("data.json")
        fun getData(): Call<GetDataResponse>
    }
}

/* Response Catalog */
data class GetDataResponse(@SerializedName("Brastlewark") val brastlewark: List<ProfileResponse>)

data class ProfileResponse(@SerializedName("id") val id: Long,
                           @SerializedName("name") val name: String,
                           @SerializedName("thumbnail") val thumbnail: String,
                           @SerializedName("age") val age: Int,
                           @SerializedName("weight") val weight: Float,
                           @SerializedName("height") val height: Float,
                           @SerializedName("hair_color") val hairColor: String,
                           @SerializedName("professions") val professions: List<String>,
                           @SerializedName("friends") val friends: List<String>)