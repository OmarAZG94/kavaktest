package com.mtg.gnoming.modules.profiles

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.mtg.gnoming.Application
import com.mtg.gnoming.db.entities.Profiles

class ProfileModel : ViewModel() {

    fun getProfiles(): LiveData<List<Profiles>> = Application.getDatabase().profilesDao().selectAll()
}