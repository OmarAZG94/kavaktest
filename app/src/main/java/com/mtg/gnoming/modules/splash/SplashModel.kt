package com.mtg.gnoming.modules.splash

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mtg.gnoming.Application
import com.mtg.gnoming.Application.Companion.TAG
import com.mtg.gnoming.db.entities.Profiles
import com.mtg.gnoming.db.managers.FriendsMgr
import com.mtg.gnoming.db.managers.ProfessionsMgr
import com.mtg.gnoming.db.managers.ProfilesMgr
import com.mtg.gnoming.net.GetDataResponse
import com.mtg.gnoming.utils.toFriendsList
import com.mtg.gnoming.utils.toProfessionsList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SplashModel : ViewModel() {

    /* Download All Data From Server */
    fun downloadData(): LiveData<GetDataResponse?> {
        val liveData = MutableLiveData<GetDataResponse?>()
        Application.getNetworkApi().getData().enqueue(object : Callback<GetDataResponse> {
            override fun onResponse(call: Call<GetDataResponse>, response: Response<GetDataResponse>) {
                if (response.isSuccessful) {
                    val result = response.body()
                    // Insert data in database
                    result?.apply {
                        this.brastlewark.forEach {
                            ProfilesMgr.insertProfile(Profiles(it.id, it.name, it.thumbnail, it.age, it.weight, it.height, it.hairColor))
                            ProfessionsMgr.insertProfession(it.professions.toProfessionsList(it.id))
                            FriendsMgr.insertFriends(it.friends.toFriendsList(it.id))
                        }
                    }
                    liveData.value = result
                } else {
                    Log.w(TAG, "Error downloading data: Code=${response.code()}")
                    liveData.value = null
                }
            }

            override fun onFailure(call: Call<GetDataResponse>, t: Throwable) {
                Log.w(TAG, "Error downloading data: ${t.message}")
                liveData.value = null
            }
        })
        return liveData
    }
}