package com.mtg.gnoming.modules.profiles

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mtg.gnoming.R
import com.mtg.gnoming.databinding.FragmentProfilesBinding

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class ProfilesFragment : Fragment() {

    private lateinit var binding: FragmentProfilesBinding
    private lateinit var viewModel: ProfileModel
    private lateinit var adapter: ProfileAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profiles, container, false)
        viewModel = ViewModelProvider(this).get(ProfileModel::class.java)
        binding.rcvProfiles.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = ProfileAdapter()
        binding.rcvProfiles.adapter = adapter
        viewModel.getProfiles().observe(viewLifecycleOwner, {
            adapter.submitList(it)
        })
    }
}