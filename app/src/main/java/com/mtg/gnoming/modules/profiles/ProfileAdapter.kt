package com.mtg.gnoming.modules.profiles


import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.mtg.gnoming.Application.Companion.TAG
import com.mtg.gnoming.R
import com.mtg.gnoming.databinding.ItemProfileBinding
import com.mtg.gnoming.db.entities.Friends
import com.mtg.gnoming.db.entities.Profiles
import com.mtg.gnoming.db.managers.FriendsMgr
import com.mtg.gnoming.db.managers.ProfessionsMgr
import com.mtg.gnoming.utils.BindingViewHolder
import com.mtg.gnoming.utils.toFriendsDescription
import com.mtg.gnoming.utils.toProfessionsDescription
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import java.lang.Exception

class ProfileAdapter : ListAdapter<Profiles, BindingViewHolder<ItemProfileBinding>>(ProfileDiffCallback()) {

    private val picasso: Picasso = Picasso.get()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder<ItemProfileBinding> {
        return BindingViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_profile, parent, false))
    }

    override fun onBindViewHolder(holder: BindingViewHolder<ItemProfileBinding>, position: Int) {
        val item = getItem(position)
        getItem(position).professions = ProfessionsMgr.getProfessions(item.id).toProfessionsDescription()
        getItem(position).friends = FriendsMgr.getFriends(item.id).toFriendsDescription()
        holder.getBinding().profile = item
        holder.getBinding().txtProfessions.isSelected = true
        holder.getBinding().txtFriends.isSelected = true
        picasso.load(getItem(position).thumbnail).placeholder(R.drawable.ic_launcher_foreground).into(holder.getBinding().imgProfile, object : Callback {
            override fun onError(e: Exception?) {
                Log.w(TAG, "Picasso Error: ${e?.message}")
                // TODO: Validar porqué arroja un estatus 503 a la hora de consumir las imágenes
            }

            override fun onSuccess() {}
        })
    }

    override fun onViewRecycled(holder: BindingViewHolder<ItemProfileBinding>) {
        super.onViewRecycled(holder)
        Picasso.get().cancelRequest(holder.getBinding().imgProfile)
    }
}

class ProfileDiffCallback : DiffUtil.ItemCallback<Profiles>() {
    override fun areItemsTheSame(oldItem: Profiles, newItem: Profiles): Boolean = oldItem == newItem
    override fun areContentsTheSame(oldItem: Profiles, newItem: Profiles): Boolean = oldItem == newItem
}