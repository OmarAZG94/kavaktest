package com.mtg.gnoming.modules.splash

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.Button
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.snackbar.Snackbar
import com.mtg.gnoming.R
import com.mtg.gnoming.databinding.FragmentSplashBinding

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class SplashFragment : Fragment() {

    private lateinit var binding: FragmentSplashBinding
    private lateinit var viewModel: SplashModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash, container, false)
        viewModel = ViewModelProvider(this).get(SplashModel::class.java)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.downloadData().observe(viewLifecycleOwner, {
            if (it == null) {
                binding.pgbSplash.visibility = GONE
                Snackbar.make(binding.root, "Can't retrieve data", Snackbar.LENGTH_SHORT).show()
                Handler(Looper.getMainLooper()).postDelayed({ findNavController().navigate(R.id.closeSplash) }, 1500)
            } else {
                Handler(Looper.getMainLooper()).postDelayed({ findNavController().navigate(R.id.closeSplash) }, 2000)
            }
        })
    }
}