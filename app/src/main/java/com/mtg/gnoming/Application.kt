package com.mtg.gnoming

import android.annotation.SuppressLint
import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.mtg.gnoming.db.AppDatabase
import com.mtg.gnoming.net.NetworkApi

@SuppressLint("StaticFieldLeak")
class Application : MultiDexApplication() {

    init {
        instance = this
    }

    companion object {
        val TAG = "Gnoming Application"
        private var instance: Application? = null
        private var database: AppDatabase? = null
        private var networkApi: NetworkApi.ApiService? = null

        fun getContext(): Context = instance!!.applicationContext
        fun getDatabase(): AppDatabase = database!!
        fun getNetworkApi(): NetworkApi.ApiService = networkApi!!
    }

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(getContext())
        database = AppDatabase.getInMemoryDatabase(getContext())
        networkApi = NetworkApi().getNetworkService()
    }
}