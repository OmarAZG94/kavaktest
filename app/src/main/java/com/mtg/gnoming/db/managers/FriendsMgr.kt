package com.mtg.gnoming.db.managers

import com.mtg.gnoming.Application
import com.mtg.gnoming.db.entities.Friends
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

open class FriendsMgr {
    companion object {
        fun insertFriends(friends: List<Friends>) = GlobalScope.launch {
            if (friends.isNotEmpty()) {
                Application.getDatabase().friendsDao().deleteByIdProfile(friends.first().idProfile)
                Application.getDatabase().friendsDao().insertAll(friends)
            }
        }

        fun getFriends(idProfile: Long): List<String> = runBlocking {
            return@runBlocking Application.getDatabase().friendsDao().selectAll(idProfile)
        }
    }
}