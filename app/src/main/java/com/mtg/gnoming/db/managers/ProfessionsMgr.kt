package com.mtg.gnoming.db.managers

import com.mtg.gnoming.Application
import com.mtg.gnoming.db.entities.Professions
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

open class ProfessionsMgr {
    companion object {
        fun insertProfession(professions: List<Professions>) = GlobalScope.launch {
            if (professions.isNotEmpty()) {
                Application.getDatabase().professionsDao()
                    .deleteByIdProfile(professions.first().idProfile)
                Application.getDatabase().professionsDao().insertAll(professions)
            }
        }

        fun getProfessions(idProfile: Long): List<String> = runBlocking {
            return@runBlocking Application.getDatabase().professionsDao().selectAll(idProfile)
        }
    }
}