package com.mtg.gnoming.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.mtg.gnoming.db.entities.Friends

@Dao
interface FriendsDao {

    @Insert(onConflict = REPLACE)
    fun insertAll(friends: List<Friends>)

    @Query("SELECT value FROM friends WHERE id_profile = :idProfile ORDER BY value ASC")
    suspend fun selectAll(idProfile: Long): List<String>

    @Query("DELETE FROM friends WHERE id_profile = :idProfile")
    fun deleteByIdProfile(idProfile: Long)
}