package com.mtg.gnoming.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "profiles")
data class Profiles(@PrimaryKey(autoGenerate = false) val id: Long,
                    @ColumnInfo(name = "name") val name: String,
                    @ColumnInfo(name = "thumbnail") val thumbnail: String,
                    @ColumnInfo(name = "age") val age: Int,
                    @ColumnInfo(name = "weight") val weight: Float,
                    @ColumnInfo(name = "height") val height: Float,
                    @ColumnInfo(name = "hair_color") val hairColor: String) {
    @Ignore
    var professions: String = ""
    @Ignore
    var friends: String = ""
}
