package com.mtg.gnoming.db.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.mtg.gnoming.db.entities.Profiles

@Dao
interface ProfilesDao {
    @Insert(onConflict = REPLACE)
    suspend fun insert(profiles: Profiles)

    @Query("SELECT * FROM profiles ORDER BY name ASC")
    fun selectAll(): LiveData<List<Profiles>>
}