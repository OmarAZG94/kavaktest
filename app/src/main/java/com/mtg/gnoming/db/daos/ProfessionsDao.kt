package com.mtg.gnoming.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.mtg.gnoming.db.entities.Professions

@Dao
interface ProfessionsDao {

    @Insert(onConflict = REPLACE)
    fun insertAll(professions: List<Professions>)

    @Query("SELECT value FROM professions WHERE id_profile = :idProfile ORDER BY value ASC")
    suspend fun selectAll(idProfile: Long): List<String>

    @Query("DELETE FROM professions WHERE id_profile = :idProfile")
    fun deleteByIdProfile(idProfile: Long)
}