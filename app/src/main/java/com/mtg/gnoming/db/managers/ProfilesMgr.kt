package com.mtg.gnoming.db.managers

import com.mtg.gnoming.Application
import com.mtg.gnoming.db.entities.Profiles
import kotlinx.coroutines.runBlocking


open class ProfilesMgr {
    companion object {
        fun insertProfile(profiles: Profiles) = runBlocking {
            Application.getDatabase().profilesDao().insert(profiles)
        }
    }
}