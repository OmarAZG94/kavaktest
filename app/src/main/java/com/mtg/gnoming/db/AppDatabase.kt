package com.mtg.gnoming.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.mtg.gnoming.db.daos.FriendsDao
import com.mtg.gnoming.db.daos.ProfessionsDao
import com.mtg.gnoming.db.daos.ProfilesDao
import com.mtg.gnoming.db.entities.Friends
import com.mtg.gnoming.db.entities.Professions
import com.mtg.gnoming.db.entities.Profiles

private const val DB_NAME = "gnoming"
private const val DB_VERSION = 1

@Database(entities = [Friends::class,
    Professions::class,
    Profiles::class], version = DB_VERSION, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun friendsDao(): FriendsDao
    abstract fun professionsDao(): ProfessionsDao
    abstract fun profilesDao(): ProfilesDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        @JvmStatic
        fun getInMemoryDatabase(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class.java) {
                    INSTANCE = Room.databaseBuilder(context, AppDatabase::class.java, DB_NAME).build()
                }
            }
            return INSTANCE
        }
    }
}