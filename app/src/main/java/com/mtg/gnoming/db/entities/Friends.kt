package com.mtg.gnoming.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "friends")
data class Friends(@PrimaryKey(autoGenerate = true) val id: Long,
                   @ColumnInfo(name = "id_profile") val idProfile: Long,
                   @ColumnInfo(name = "value") val value: String)
