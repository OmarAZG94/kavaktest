package com.mtg.gnoming.utils

import com.mtg.gnoming.db.entities.Friends
import com.mtg.gnoming.db.entities.Professions

fun List<String>.toFriendsList(idProfile: Long): List<Friends> {
    val list = mutableListOf<Friends>()
    forEach { list.add(Friends(0, idProfile, it)) }
    return list
}

fun List<String>.toProfessionsList(idProfile: Long): List<Professions> {
    val list = mutableListOf<Professions>()
    forEach { list.add(Professions(0, idProfile, it)) }
    return list
}

fun List<String>.toProfessionsDescription(): String {
    var description = if (isEmpty()) "No Professions Registered" else "Professions: "
    forEach {
        description += if (it == this.first()) it
        else ", $it"
    }
    return description
}

fun List<String>.toFriendsDescription(): String {
    var description = if (isEmpty()) "No Friends Registered" else "Friends: "
    forEach {
        description += if (it == this.first()) it
        else ", $it"
    }
    return description
}